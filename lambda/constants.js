module.exports = {
    // these are the permissions needed to fetch the first name
    GIVEN_NAME_PERMISSION: ['alexa::profile:given_name:read'],
    APL: {
        stepsDetailDoc: require('./documents/stepsDetail/document.json'),
        patternCarouselDoc: require('./documents/patternCarousel/document.json'),
        launchAnimatedDoc: require('./documents/launchAnimated/document.json'),
        stepsVideoDetail: require('./documents/stepsVideoDetail/document.json'),
    }
}
