// const moment = require('moment-timezone'); // will help us do all the dates math while considering the timezone
// const util = require('./util');
const axios = require("axios");

module.exports = {
  fetchPatternSteps(data) {
    const endpoint = "http://159.89.167.87/accuquilt/api";
    const url = `${endpoint}/patternSteps`;

    console.log(url); // in case you want to try the query in a web browser

    var config = {
      timeout: 6500, // timeout api call before we reach Alexa's 8 sec timeout, or set globally via axios.defaults.timeout
      // headers: { 'Accept': 'application/sparql-results+json' }
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };

    async function getJsonResponse(url, data, config) {
      const res = await axios.post(url, data, config);
      return res.data;
    }

    return getJsonResponse(url, data, config)
      .then((result) => {
        return result;
      })
      .catch((error) => {
        return null;
      });
  },
  fetchQuestionAnswer(data) {
    const endpoint = "http://159.89.167.87/accuquilt/api";
    const url = `${endpoint}/questionAnswer`;

    var config = {
      timeout: 6500, // timeout api call before we reach Alexa's 8 sec timeout, or set globally via axios.defaults.timeout
      // headers: { 'Accept': 'application/sparql-results+json' }
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };

    async function getJsonResponse(url, data, config) {
      const res = await axios.post(url, data, config);
      return res.data;
    }

    return getJsonResponse(url, data, config)
      .then((result) => {
        return result;
      })
      .catch((error) => {
        return null;
      });
  },
};
