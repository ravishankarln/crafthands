/*
 * Copyright 2018-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

// sets up dependencies
const Alexa = require("ask-sdk-core");
const apiService = require("./apiServices");
const interceptors = require("./interceptors");
const util = require("./util");
const constants = require('./constants');

const { get } = require("lodash");

// load our APL and APLA documents
var deafultData = require(`./documents/stepsDetail/datasources/default.json`);
var deafultDataCarousel = require(`./documents/patternCarousel/datasources/default.json`);
var deafultDataWithVideo = require(`./documents/stepsVideoDetail/datasources/default.json`);
const nullImage = 'https://accuquiltalexa.s3.amazonaws.com/default.png'
const defaultVideo = "https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_1mb.mp4"

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    const { attributesManager, requestEnvelope } = handlerInput;
    const request = requestEnvelope.request;
    // return (
    //   Alexa.getRequestType(handlerInput.requestEnvelope) === "LaunchRequest"
    // );
    // checks request type
    return (
      request.type === "LaunchRequest"
    );
  },
  handle(handlerInput) {
    const requestAttributes = handlerInput.attributesManager.getRequestAttributes();

    let speechText = `${requestAttributes.t("WELCOME_MSG")} Do you want answers or steps?`;

    if (util.supportsAPL(handlerInput)) {
      const { Viewport } = handlerInput.requestEnvelope.context;
      const resolution = Viewport.pixelWidth + 'x' + Viewport.pixelHeight;
      handlerInput.responseBuilder.addDirective({
        type: 'Alexa.Presentation.APL.RenderDocument',
        version: '1.4',
        token: 'launchAnimatedDoc',
        document: constants.APL.launchAnimatedDoc,
        datasources: {}
      });
    }

    // we use intent chaining to trigger the birthday registration multi-turn
    return (
      handlerInput.responseBuilder
        .speak(Alexa.escapeXmlCharacters(speechText))
        // we use intent chaining to trigger the birthday registration multi-turn
        // .addDelegateDirective({
        //   name: "ChooseIntent",
        //   confirmationStatus: "NONE",
        //   slots: {},
        // })
        .getResponse()
    );
  },
};

const ChooseIntentHandler = {
  canHandle(handlerInput) {
    const { attributesManager, requestEnvelope } = handlerInput;
    const request = requestEnvelope.request;

    return (
      request.type === "IntentRequest" &&
      request.intent.name === "ChooseIntent"
    );
  },
  handle(handlerInput) {
    const { attributesManager, requestEnvelope } = handlerInput;
    console.log('requestEnvelope', requestEnvelope)
    const requestAttributes = attributesManager.getRequestAttributes();

    const choice = Alexa.getSlotValue(requestEnvelope, "choice");

    let speechText = 'Okay, ask me about any pattern, for example, How do I make a Sea Life Medley wall hanging pattern'
    let intentDelegate = 'AskForStepsIntent'
    console.log(`choice intent`, choice)
    if (!choice.toLowerCase().trim().includes('step')) {
      speechText = "Okay, ask me any question you have, for example, what is rotary cutter?"
      intentDelegate = 'AskQuestionIntent'
    }

    // if (util.supportsAPL(handlerInput)) {
    //   const { Viewport } = handlerInput.requestEnvelope.context;
    //   const resolution = Viewport.pixelWidth + 'x' + Viewport.pixelHeight;
    //   handlerInput.responseBuilder.addDirective({
    //     type: 'Alexa.Presentation.APL.RenderDocument',
    //     version: '1.4',
    //     token: 'launchAnimatedDoc',
    //     document: constants.APL.launchAnimatedDoc,
    //     datasources: {}
    //   });
    // }

    // we use intent chaining to trigger the birthday registration multi-turn
    return (
      handlerInput.responseBuilder
        .speak(Alexa.escapeXmlCharacters(speechText))
        // we use intent chaining to trigger the birthday registration multi-turn
        .addDelegateDirective({
          name: intentDelegate,
          confirmationStatus: "NONE",
          slots: {},
        })
        .getResponse()
    );
  },
};

// core functionality for fact skill
const AskForStepsIntentHandler = {
  canHandle(handlerInput) {
    const { attributesManager, requestEnvelope } = handlerInput;
    // the attributes manager allows us to access session attributes
    const sessionAttributes = attributesManager.getSessionAttributes();
    const { intent } = requestEnvelope.request;
    const request = requestEnvelope.request;

    // checks request type
    return (
      request.type === "IntentRequest" &&
      request.intent.name === "AskForStepsIntent" ||
      request.type === 'Alexa.Presentation.APL.UserEvent' &&
      request.arguments[0] === 'tap_step'
    );
  },
  async handle(handlerInput) {
    const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    // gets a random fact by assigning an array to the variable
    // the random item from the array will be selected by the i18next library
    // the i18next library is set up in the Request Interceptor
    const { attributesManager, requestEnvelope } = handlerInput;
    const sessionAttributes = attributesManager.getSessionAttributes();
    sessionAttributes["sessionCounterX"] = sessionAttributes["sessionCounterX"]
      ? sessionAttributes["sessionCounterX"] + 1
      : 0;

    console.log('sessionAttributes["sessionCounterX"]', sessionAttributes["sessionCounterX"])
    const model = Alexa.getSlotValue(requestEnvelope, "model");

    const data = {
      data: {
        keywords: `${model}`.replace(/[ ,]+/g, ","),
      },
    };

    console.log(`data request params`, data)

    const response = await apiService.fetchPatternSteps(data);
    console.log("fetchPatternSteps", JSON.stringify(response));


    sessionAttributes["stepsResponse"] = response;

    const length = response.data.length;
    const randomFactArr = get(response, `data`, [])
    const speechArr = randomFactArr.map(item => `${item.pattern_name}`.replace(/!/g, ''))

    const patternCarouselData = randomFactArr.map((item, indx) => {
      return {
        "primaryText": `${item.pattern_name}`.replace(/!/g, ''),
        "number": `${indx + 1}`,
        "imageSource": `${item.pattern_image}`,
      }
    })

    var speakOutputX = ''
    for (let idx = 0; idx < speechArr.length; idx++) {
      const element = speechArr[idx];
      speakOutputX += `${idx + 1}. ${element}.   `
    }

    // concatenates a standard message with the random fact
    // const speakOutput = speakOutputX ? requestAttributes.t("GET_FACT_MESSAGE") + speakOutputX : 'Sorry, we could not find anything';
    const speakOutput = speakOutputX ? 'These are the patterns i found, let me know which one you want by telling the number.  ' + speakOutputX + '. ' + '  That\'s it. Which one do you want?' : 'Sorry, we could not find anything';

    // Add APL directive to response
    if (util.supportsAPL(handlerInput)) {
      const { Viewport } = handlerInput.requestEnvelope.context;
      const resolution = Viewport.pixelWidth + 'x' + Viewport.pixelHeight;
      handlerInput.responseBuilder.addDirective({
        type: 'Alexa.Presentation.APL.RenderDocument',
        version: '1.6',
        token: 'patternCarouselDoc',
        document: constants.APL.patternCarouselDoc,
        datasources: {
          imageListData: {
            ...deafultDataCarousel.imageListData,
            "listItems": patternCarouselData
          },
        }
      });
    }

    return (
      handlerInput.responseBuilder
        .speak(Alexa.escapeXmlCharacters(speakOutput))
        // Uncomment the next line if you want to keep the session open so you can
        // ask for another fact without first re-opening the skill
        .reprompt(requestAttributes.t('HELP_REPROMPT'))
        .withSimpleCard(requestAttributes.t("SKILL_NAME"), speakOutputX)
        .getResponse()
    );
  },
};

const AskQuestionIntentHandler = {
  canHandle(handlerInput) {
    const { attributesManager, requestEnvelope } = handlerInput;
    // the attributes manager allows us to access session attributes
    const sessionAttributes = attributesManager.getSessionAttributes();
    const { intent } = requestEnvelope.request;
    const request = requestEnvelope.request;

    // checks request type
    return (
      request.type === "IntentRequest" &&
      request.intent.name === "AskQuestionIntent" ||
      request.type === 'Alexa.Presentation.APL.UserEvent' &&
      request.arguments[0] === 'tap_answer'
    );
  },
  async handle(handlerInput) {
    const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    // gets a random fact by assigning an array to the variable
    // the random item from the array will be selected by the i18next library
    // the i18next library is set up in the Request Interceptor
    const { requestEnvelope } = handlerInput;
    const model = Alexa.getSlotValue(requestEnvelope, "model");

    console.log(`modelX`, model)

    const data = {
      data: {
        keywords: `${model}`.replace(/[ ,]+/g, ",")
      },
    };

    console.log(`data request params`, data)

    const response = await apiService.fetchQuestionAnswer(data);
    console.log("fetchQuestionAnswer", JSON.stringify(response));

    const randomFact = response && response.data.length && response.data[0] && response.data[0].answer || '';
    // concatenates a standard message with the random fact
    const speakOutput = randomFact ? requestAttributes.t("GET_FACT_MESSAGE") + randomFact : 'Sorry, we could not find anything'

    return (
      handlerInput.responseBuilder
        .speak(Alexa.escapeXmlCharacters(speakOutput))
        // Uncomment the next line if you want to keep the session open so you can
        // ask for another fact without first re-opening the skill
        .reprompt(requestAttributes.t('HELP_REPROMPT'))
        .withSimpleCard(requestAttributes.t("SKILL_NAME"), randomFact)
        .getResponse()
    );
  },
};

const SelectPatternIntentHandler = {
  canHandle(handlerInput) {
    const { attributesManager, requestEnvelope } = handlerInput;
    // the attributes manager allows us to access session attributes
    const sessionAttributes = attributesManager.getSessionAttributes();
    const { intent } = requestEnvelope.request;
    const request = requestEnvelope.request;

    console.log(`request.arguments`, request.arguments)

    // checks request type
    return (
      request.type === "IntentRequest" &&
      request.intent.name === "SelectPatternIntent" ||
      request.type === 'Alexa.Presentation.APL.UserEvent' &&
      request.arguments[0] === 'patternSelect'
    );
  },
  handle(handlerInput) {

    const { attributesManager, requestEnvelope } = handlerInput;
    const request = requestEnvelope.request;
    const requestAttributes = attributesManager.getRequestAttributes();
    const sessionAttributes = attributesManager.getSessionAttributes();


    let number = Alexa.getSlotValue(requestEnvelope, "number");

    if (request.type === 'Alexa.Presentation.APL.UserEvent' &&
      request.arguments[0] === 'patternSelect') {
      number = parseInt(request.arguments[1])
    }

    const sessionCounterX = sessionAttributes["sessionCounterX"];
    const stepsResponse = sessionAttributes["stepsResponse"];

    console.log('number', number)
    console.log('sessionCounterX', sessionCounterX)
    console.log(`stepsResponse`, stepsResponse)

    sessionAttributes["patternNumber"] = number;

    sessionAttributes["selectedPatternName"] = get(stepsResponse, `data.${number - 1}.pattern_name`, 'Unknown').replace(/!/g, '')
    sessionAttributes["selectedPatternRequirement"] = get(stepsResponse, `data.${number - 1}.requirements`, 'Unknown')
    sessionAttributes["selectedPatternImage"] = get(stepsResponse, `data.${number - 1}.pattern_image`, nullImage)
    sessionAttributes["selectedPatternVideo"] = get(stepsResponse, `data.${number - 1}.pattern_video`, '')

    const currentStepsToSpeech = get(stepsResponse, `data.${number - 1}.get_steps`, [])

    const speechArr = currentStepsToSpeech.map((item, index) => {
      return {
        step: `${index + 1}. ${item.step}.    `.replace(/!/g, ''),
        vStep: `${index + 1}. ${item.visual_step}.    `.replace(/!/g, ''),
        // material: item.material_name ? item.material_name : "",
        material: item.material_name ? Alexa.escapeXmlCharacters(item.material_name) : '',
        image: item.step_image ? item.step_image : "",
        video: item.step_video ? item.step_video : sessionAttributes["selectedPatternVideo"],
        extraImages: item.get_step_images.map((itmImage, imgIdx) => {
          return {
            "thumbImageSource": itmImage.step_image,
            "id": itmImage.pattern_step_image_id
          }
        })
      }
    })
    // const speechArrVisual = currentStepsToSpeech.map(item => item.visual_step)
    // const arrMaterialName = currentStepsToSpeech.map(item => item.material_name)
    let patternRequirementX = sessionAttributes["selectedPatternRequirement"];

    const requirementStep = {
      step: `${Alexa.escapeXmlCharacters(patternRequirementX)}`.replace(/!/g, ''),
      vStep: sessionAttributes["selectedPatternRequirement"],
      material: "",
      image: sessionAttributes["selectedPatternImage"],
      video: sessionAttributes["selectedPatternVideo"],
      extraImages: [{
        "thumbImageSource": sessionAttributes["selectedPatternImage"],
        "id": 'patternImage'
      }]
    }

    var allSteps = [requirementStep].concat(speechArr);
    // var allSteps = speechArr

    console.log(`allStepsX`, allSteps.length);
    // for (let idx = 0; idx < speechArr.length; idx++) {
    //   const element = speechArr[idx];
    //   allSteps.push(`${idx + 1}. ${element.step}.    `)
    // }

    sessionAttributes["allSteps"] = allSteps;

    console.log(`allStepsX 2`, allSteps.length);

    const currentStepIndex = 0
    sessionAttributes['currentStep'] = currentStepIndex;

    // concatenates a standard message with the random fact
    const speakOutput = `Okay , you have selected number ${number}, here are the required materiels you will need to have.`;

    if (allSteps.length) {
      // we can't use intent chaining because the target intent is not dialog based
      return NextStepIntentHandler.handle(handlerInput);
    }

    return (
      handlerInput.responseBuilder
        .speak(Alexa.escapeXmlCharacters(speakOutput))
        // Uncomment the next line if you want to keep the session open so you can
        // ask for another fact without first re-opening the skill
        .reprompt(requestAttributes.t('HELP_REPROMPT'))
        // .withSimpleCard(requestAttributes.t("SKILL_NAME"), randomFact)
        .getResponse()
    );
  },
};

const NextStepIntentHandler = {
  canHandle(handlerInput) {
    const { attributesManager, requestEnvelope } = handlerInput;
    // the attributes manager allows us to access session attributes
    const sessionAttributes = attributesManager.getSessionAttributes();
    const { intent } = requestEnvelope.request;
    const request = requestEnvelope.request;

    console.log(`request arguments`, request);

    // checks request type
    return (
      request.type === "IntentRequest" &&
      request.intent.name === "NextStepIntent" ||
      request.type === 'Alexa.Presentation.APL.UserEvent' &&
      request.arguments[0] === 'Next'

    );
  },
  handle(handlerInput) {

    const { attributesManager, requestEnvelope } = handlerInput;

    const requestAttributes = attributesManager.getRequestAttributes();
    const sessionAttributes = attributesManager.getSessionAttributes();

    const allSteps = sessionAttributes["allSteps"]
    const patternName = sessionAttributes["selectedPatternName"]
    const patternRequirement = sessionAttributes["selectedPatternRequirement"]
    const patternImage = sessionAttributes["selectedPatternImage"]

    let currentStepIndex = sessionAttributes['currentStep']

    console.log(`allSteps`, allSteps)
    console.log(`currentStepIndex`, currentStepIndex)

    var speakOutputX = ''
    var speakOutputXDisplay = ''

    if (currentStepIndex === 0) {
      speakOutputX = `Okay , you have selected ${patternName}, here are the required materiels you will need to have. ${allSteps[currentStepIndex].step}`
      speakOutputXDisplay = `Okay , you have selected ${patternName}, here are the required materiels you will need to have. ${allSteps[currentStepIndex].vStep}`
    } else {
      speakOutputX = allSteps[currentStepIndex].step
      speakOutputXDisplay = allSteps[currentStepIndex].vStep
    }


    if (currentStepIndex >= allSteps.length - 1) {
      speakOutputX = `${allSteps[currentStepIndex].step}. That's it, This was the last step. Enjoy your pattern. Goodbye`
      speakOutputXDisplay = `${allSteps[currentStepIndex].vStep}. That's it, This was the last step. Enjoy your pattern. Goodbye`
    }

    // concatenates a standard message with the random fact
    const speakOutput = speakOutputX;

    // Add APL directive to response
    if (util.supportsAPL(handlerInput)) {
      const { Viewport } = handlerInput.requestEnvelope.context;
      const resolution = Viewport.pixelWidth + 'x' + Viewport.pixelHeight;
      handlerInput.responseBuilder.addDirective({
        type: 'Alexa.Presentation.APL.RenderDocument',
        version: '1.4',
        token: 'stepsVideoDetailDoc',
        document: constants.APL.stepsVideoDetail,
        datasources: {
          ...deafultDataWithVideo,
          "templateData": {
            "type": "object",
            "properties": {
              "item": {
                "runtimeMinutes": "",
                "overview": speakOutputXDisplay,
                "materialNames": `${allSteps[currentStepIndex].material}`,
                "videoOverlaySource": "/EmptyPng?quality=90",
                "primaryImageSource": patternImage,
                "type": "",
                "logoImageSource": "https://www.accuquilt.com/media/logo/websites/1/accuquilt-logo-no-tagline-4c-948x176-NEW.png",
                "videoBackdropSource": allSteps[currentStepIndex].video,
                "premiereDate": "",
                "officialRating": "",
                "genres": "",
                "patternName": patternName,
                "backdropImageSource": "https://i.ibb.co/CJFwnnG/Alexa-Skill-Background-480x480.jpg",
                "backgroundSource": "https://i.ibb.co/37bvXV6/Alexa-Skill-Background-1280x800.jpg",
                "endTime": "",
                "id": 34516
              },
              "similarItems": allSteps[currentStepIndex].extraImages,
              "url": "https://theater.unityhome.online"
            }
          },
          detailImageRightData: {
            image: {
              contentDescription: `${allSteps[currentStepIndex].material}`,
              smallSourceUrl: null,
              largeSourceUrl: null,
              sources: [
                {
                  url: `${allSteps[currentStepIndex].image}`,
                  size: "small",
                  widthPixels: 0,
                  heightPixels: 0
                },
                {
                  url: `${allSteps[currentStepIndex].image}`,
                  size: "large",
                  widthPixels: 0,
                  heightPixels: 0
                }
              ]
            },
          },
        }
      });
    }

    currentStepIndex = ++sessionAttributes['currentStep']

    return (
      handlerInput.responseBuilder
        .speak(Alexa.escapeXmlCharacters(speakOutput))
        // Uncomment the next line if you want to keep the session open so you can
        // ask for another fact without first re-opening the skill
        // .reprompt(requestAttributes.t('HELP_REPROMPT'))
        .withSimpleCard(requestAttributes.t("SKILL_NAME"), speakOutputX)
        .getResponse()
    );
  },
};

const RepeatIntentHandler = {
  canHandle(handlerInput) {
    const { attributesManager, requestEnvelope } = handlerInput;
    // the attributes manager allows us to access session attributes
    const sessionAttributes = attributesManager.getSessionAttributes();
    const { intent } = requestEnvelope.request;
    const request = requestEnvelope.request;

    // checks request type
    return (
      request.type === "IntentRequest" &&
      request.intent.name === "RepeatIntent" ||
      request.type === 'Alexa.Presentation.APL.UserEvent' &&
      request.arguments[0] === 'Repeat'
    );
  },
  handle(handlerInput) {

    const { attributesManager, requestEnvelope } = handlerInput;

    const requestAttributes = attributesManager.getRequestAttributes();
    const sessionAttributes = attributesManager.getSessionAttributes();

    const allSteps = sessionAttributes["allSteps"]
    let currentStepIndex = sessionAttributes['currentStep']

    currentStepIndex = sessionAttributes['currentStep']--;

    console.log(`allSteps REPEAT`, allSteps)
    console.log(`currentStepIndex REPEAT`, currentStepIndex)

    // var speakOutputX = ''

    // speakOutputX = allSteps[currentStepIndex]

    // if (currentStepIndex === 0) {
    //   speakOutputX = `we have not started yet but here is the first step. ${allSteps[currentStepIndex]}`
    // }


    // if (currentStepIndex >= allSteps.length - 1) {
    //   speakOutputX = 'This was the last step in this pattern. Enjoy your Pattern!'
    // }

    // concatenates a standard message with the random fact
    // const speakOutput = speakOutputX;

    // currentStepIndex = ++sessionAttributes['currentStep']

    return NextStepIntentHandler.handle(handlerInput);

    return (
      handlerInput.responseBuilder
        .speak(Alexa.escapeXmlCharacters(speakOutput))
        // Uncomment the next line if you want to keep the session open so you can
        // ask for another fact without first re-opening the skill
        .reprompt(requestAttributes.t('HELP_REPROMPT'))
        .withSimpleCard(requestAttributes.t("SKILL_NAME"), speakOutputX)
        .getResponse()
    );
  },
};

const PreviousIntentHandler = {
  canHandle(handlerInput) {
    const { attributesManager, requestEnvelope } = handlerInput;
    // the attributes manager allows us to access session attributes
    const sessionAttributes = attributesManager.getSessionAttributes();
    const { intent } = requestEnvelope.request;
    const request = requestEnvelope.request;

    // checks request type
    return (
      request.type === "IntentRequest" &&
      request.intent.name === "PreviousIntent"
      ||
      request.type === 'Alexa.Presentation.APL.UserEvent' &&
      request.arguments[0] === 'Previous'
    );
  },
  handle(handlerInput) {

    const { attributesManager, requestEnvelope } = handlerInput;

    const requestAttributes = attributesManager.getRequestAttributes();
    const sessionAttributes = attributesManager.getSessionAttributes();

    const allSteps = sessionAttributes["allSteps"]
    let currentStepIndex = sessionAttributes['currentStep']

    currentStepIndex = currentStepIndex - 2
    sessionAttributes['currentStep'] = currentStepIndex

    console.log(`allSteps REPEAT`, allSteps)
    console.log(`currentStepIndex REPEAT`, currentStepIndex)

    // var speakOutputX = ''

    // speakOutputX = allSteps[currentStepIndex]

    // if (currentStepIndex === 0) {
    //   speakOutputX = `we have not started yet but here is the first step. ${allSteps[currentStepIndex]}`
    // }


    // if (currentStepIndex >= allSteps.length - 1) {
    //   speakOutputX = 'This was the last step in this pattern. Enjoy your Pattern!'
    // }

    // concatenates a standard message with the random fact
    // const speakOutput = speakOutputX;

    // currentStepIndex = ++sessionAttributes['currentStep']

    return NextStepIntentHandler.handle(handlerInput);

    return (
      handlerInput.responseBuilder
        .speak(Alexa.escapeXmlCharacters(speakOutput))
        // Uncomment the next line if you want to keep the session open so you can
        // ask for another fact without first re-opening the skill
        .reprompt(requestAttributes.t('HELP_REPROMPT'))
        .withSimpleCard(requestAttributes.t("SKILL_NAME"), speakOutputX)
        .getResponse()
    );
  },
};

const HelpHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (
      request.type === "IntentRequest" &&
      request.intent.name === "AMAZON.HelpIntent"
    );
  },
  handle(handlerInput) {
    const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    return handlerInput.responseBuilder
      .speak(Alexa.escapeXmlCharacters(requestAttributes.t("HELP_MESSAGE")))
      .reprompt(requestAttributes.t("HELP_REPROMPT"))
      .getResponse();
  },
};

const FallbackHandler = {
  // The FallbackIntent can only be sent in those locales which support it,
  // so this handler will always be skipped in locales where it is not supported.
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (
      request.type === "IntentRequest" &&
      request.intent.name === "AMAZON.FallbackIntent"
    );
  },
  handle(handlerInput) {
    const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    return handlerInput.responseBuilder
      .speak(Alexa.escapeXmlCharacters(requestAttributes.t("FALLBACK_MESSAGE")))
      .reprompt(requestAttributes.t("FALLBACK_REPROMPT"))
      .getResponse();
  },
};

const ExitHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (
      request.type === "IntentRequest" &&
      (request.intent.name === "AMAZON.CancelIntent" ||
        request.intent.name === "AMAZON.StopIntent")
    );
  },
  handle(handlerInput) {
    const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    return handlerInput.responseBuilder
      .speak(Alexa.escapeXmlCharacters(requestAttributes.t("STOP_MESSAGE")))
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === "SessionEndedRequest";
  },
  handle(handlerInput) {
    console.log(
      `Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`
    );
    return handlerInput.responseBuilder.getResponse();
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);
    console.log(`Error stack: ${error.stack}`);
    const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    return handlerInput.responseBuilder
      .speak(Alexa.escapeXmlCharacters(requestAttributes.t("ERROR_MESSAGE")))
      .reprompt(requestAttributes.t("ERROR_MESSAGE"))
      .getResponse();
  },
};

const skillBuilder = Alexa.SkillBuilders.custom();

exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    ChooseIntentHandler,
    AskForStepsIntentHandler,
    AskQuestionIntentHandler,
    SelectPatternIntentHandler,
    NextStepIntentHandler,
    PreviousIntentHandler,
    RepeatIntentHandler,
    HelpHandler,
    ExitHandler,
    FallbackHandler,
    SessionEndedRequestHandler
  )
  .addRequestInterceptors(
    interceptors.LocalisationRequestInterceptor,
    interceptors.LoggingRequestInterceptor
    // interceptors.LoadNameRequestInterceptor,
  )
  .addResponseInterceptors(interceptors.LoggingResponseInterceptor)
  .addErrorHandlers(ErrorHandler)
  .withApiClient(new Alexa.DefaultApiClient())
  // .withCustomUserAgent('sample/basic-fact/v2')
  .lambda();
