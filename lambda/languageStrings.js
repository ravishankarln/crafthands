/* *
 * We create a language strings object containing all of our strings.
 * The keys for each string will then be referenced in our code, e.g. handlerInput.t('WELCOME_MSG').
 * The localisation interceptor in index.js will automatically choose the strings
 * that match the request's locale.
 * */

module.exports = {
    en: {
        translation: {
            SKILL_NAME: 'Accu Quilt',
            GET_FACT_MESSAGE: 'Here\'s your answer: ',
            WELCOME_MSG: `Welcome to Accu Quilt. How can I help you today?`,
            HELP_MESSAGE: 'You can say tell me a space fact, or, you can say exit... What can I help you with?',
            HELP_REPROMPT: 'What can I help you with?',
            FALLBACK_MESSAGE: 'The Accu Quilt skill can\'t help you with that. It can help you discover steps about patterns, if you say how to make a quilt ribbon. What can I help you with?',
            FALLBACK_REPROMPT: 'What can I help you with?',
            ERROR_MESSAGE: 'Sorry, an error occurred.',
            STOP_MESSAGE: 'Goodbye!',
            FACTS:
            [
                'A year on Mercury is just 88 days long. Why ?',
                'Despite being farther from the Sun, Venus experiences higher temperatures than Mercury.',
                'On Mars, the Sun appears about half the size as it does on Earth.',
                'Jupiter has the shortest day of all the planets.',
                'The Sun is an almost perfect sphere.',
            ],
        }
    },
    hi: {
        translation: {
            SKILL_NAME: 'अंतरिक्ष facts',
            GET_FACT_MESSAGE: 'ये लीजिए आपका fact: ',
            HELP_MESSAGE: 'आप मुझे नया fact सुनाओ बोल सकते हैं या फिर exit भी बोल सकते हैं... आप क्या करना चाहेंगे?',
            HELP_REPROMPT: 'मैं आपकी किस प्रकार से सहायता कर सकती हूँ?',
            ERROR_MESSAGE: 'सॉरी, मैं वो समज नहीं पायी. क्या आप repeat कर सकते हैं?',
            STOP_MESSAGE: 'अच्छा bye, फिर मिलते हैं',
            FACTS:
            [
                'बुध गृह में एक साल में केवल अठासी दिन होते हैं',
                'सूरज से दूर होने के बावजूद, Venus का तापमान Mercury से ज़्यादा होता हैं',
                'Earth के तुलना से Mars में सूरज का size तक़रीबन आधा हैं',
                'सारे ग्रहों में Jupiter का दिन सबसे कम हैं',
                'सूरज का shape एकदम गेंद आकार में हैं'
            ],
        }
    }
}